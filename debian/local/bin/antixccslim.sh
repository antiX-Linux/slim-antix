#!/bin/bash
# File Name: slimbackground.sh
# Purpose: allow user to set slim login background
# Authors: OU812 for antiX
# Latest Change: 20 August 2008
# latest Change: 02 January 2009 and renamed antixccslim.sh
# latest Change: 06 April 2009 changed cd directory
# latest Change: 16 October 2018 changed icon path
# latest Change: 07 November 2019 accommodates multiple slim themes and
#                                 verifies selected background 
##########################################################################

put_error_msg () {
	yad --title="Slim Background Selecter - ERROR"\
	    --center\
	    --width="400"\
	    --button="gtk-close"\
	    --image="error"\
	    --text="  $1"
	return
}
	
# Check that xserver is running and user is root.
[[ $DISPLAY ]] || { echo "There is no xserver running. Exiting..." ; exit 1 ; }
if [ $(id -u) -eq 0 ]; then
    rm -f /tmp/antixccslim-variables.txt
else
    put_error_msg "You need to run this as root \! \n\n  Cannot continue"   
    exit 1
fi

export SLIMBACKGROUND='
<window title="Slim Background Selecter (png or jpg files accepted)" icon="gnome-control-center" window-position="1">

<vbox>
  <chooser>
    <height>500</height><width>600</width>
    <variable>BACKGROUND</variable>
  </chooser>

  <hbox>
    <button>
     <label>"View"</label>
	<input file icon="gtk-zoom-out"></input>
        <action>feh -g 320x240 "$BACKGROUND" </action>
    </button>

    <button>
    <label>"Commit"</label>
	<input file icon="gtk-yes"></input>
	    <action>echo "SELECTEDFILE=YES" > /tmp/antixccslim-variables.txt </action>
	<action>EXIT:close</action>
    </button>
    
    <button>
    <label>"Cancel"</label>
	<input file icon="gtk-cancel"></input>
	    <action>echo "SELECTEDFILE=NO" > /tmp/antixccslim-variables.txt </action>
	<action>EXIT:close</action>
    </button>
  </hbox>
</vbox>

</window>
'

gtkdialog --program=SLIMBACKGROUND >> /tmp/antixccslim-variables.txt
source /tmp/antixccslim-variables.txt

# Check to make sure the selected new background file exists
# then extract extension and make it lowercase.
if test -f "$BACKGROUND"; then
    EXT="${BACKGROUND##*.}"
    EXT=$(echo "$EXT" | tr A-Z a-z)
else
    put_error_msg "\n  The selected background file: $BACKGROUND\n\n  does not exist!"   
    exit 1    

fi

# Confirm the selected background file has an extension of either
# ".png" or ".jpg".  Those are the only two graphics files accepted by
# slim.  And make sure it really is an image file.
if [ "$EXT" = "png" -o "$EXT" = "jpg" ]; then
    if [ $(file "$BACKGROUND" | grep -c 'image data') -eq 0 ]; then
        put_error_msg "\n  The selected background is not an image data file"
        exit 1    
    fi
else
    put_error_msg "\n  The selected background is not a 'png' or 'jpg' image file"
    exit 1    
fi

# Get the current slim scheme being used so we know where to copy the
# selected background.
CURRTHEME=$(awk '$1 ~ /^current_theme/ {print $2}' /etc/slim.conf)
CURRPATH="/usr/share/slim/themes/$CURRTHEME"

# If the new background file is a "jpg" file then see if "background.png"
# exists for this theme.
MOVEPNG="NO"
if [ "$EXT" = "jpg" ]; then
    if test -f "$CURRPATH/background.png"; then
        MOVEPNG="YES"
    fi
fi

# If the user has selected a valid file then copy the file to the
# current theme directory as "background.jpg" or "background.png".
# If the selected file is a "jpg" file then rename "background.png"
# to "background.png~" if one exists.
# Note: If both are present, then the "png" version will be used by slim.

if [ "$SELECTEDFILE" = "YES" ]; then
	cp -b "$BACKGROUND" "$CURRPATH/background.$EXT"
	if [ $? = "0" ];then
		if [ "$MOVEPNG" = "YES" ]; then
			mv -f "$CURRPATH/background.png" "$CURRPATH/background.png~"
		fi
		MSG="\n  The selected background file has been successfully set\n\n"
		MSG="$MSG  Log out to see new background"  	 
		yad --title="Slim Background Selecter - SUCCESS"\
			--center\
			--width="400"\
            --button="gtk-ok"\
			--text="$MSG"
	else
        put_error_msg "\n  Problem occurred with setting the selected background file"
		exit 1
	fi        
fi
